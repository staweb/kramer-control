# README #

### Notes on Security ###
Access to routing pages are not secured, anyone with access to the web interface can control any inputs/outputs. Never host this server on the public internet or on an unsecured network.

### How do I get set up? ###

* Ensure you have pip installed
* Install Django `pip install django`
* Navigate to the kramer-control/kramer_web directory
* Migrate the Django project `python manage.py migrate`
* Create a superuser `python manage.py createsuperuser`
* Set the serial port in /api/settings.py
* Run the dev server `python manage.py runserver`
* Set up inputs, outputs and routing pages at localhost:8000/admin/
* View pages at localhost:8000/page/#SLUG#