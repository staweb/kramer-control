import serial
import array
import time
import threading
import time
from collections import defaultdict

class Kramer:

    # Commands from the device are dealt with in a
    # separate thread
    class Reader(threading.Thread):
        def __init__(self, kramer, *args, **kwargs):
            super(Kramer.Reader, self).__init__(*args, **kwargs)
            self.daemon = True  # we will be mercilessly killed on shutdown
            self._kramer = kramer
#            self._instruction_handlers = defaultdict( self.unknown )
#            self._instruction_handlers[1] = self.handle_switch_video
        
        def run(self):
            while 1:
                message = self._kramer.ser.read(4)
                print "Received: " + ''.join( map(lambda x: x.encode('hex'), message))
                (dest, opcode, input_, output, override, machine_no) = self._decode( message )

                if dest == 0:
                    # ignore messages sent from the pc side
                    continue

                # handle instruction
                if opcode == 1:
                    self.handle_switch_video(input_, output)
                elif opcode == 5:
                    self.handle_request_video_status(input_, output)
                else:
                    self.unknown(input_, output)

        def handle_switch_video(self, input_, output):
            self._kramer._video_state[output] = input_

        def handle_request_video_status(self, input_, output):
            print "Request video status: input=%d, output=%d" % (input_, ouput)
            self._kramer._video_state[output] = input_

        def unknown(self, input_, output):
            print "Received unknown opcode with input %d, output %s" % (input_, output)

                

        def _decode(self, instr ):
            assert len(instr) == 4
            instr = map(ord, instr)

            dest = ( instr[0] & 0x40 ) >> 6
            opcode = instr[0] & 0x3F
            input_ = instr[1] & 0x7F
            output = instr[2] & 0x7F
            override = instr[3] & 0x40 >> 6
            machine_no = instr[3] & 0x1f

            return (dest, opcode, input_, output, override, machine_no)


    def __init__(self, serial_object=None, port=None):
        # Serial
        if serial_object is not None:
            self.ser = serial_object
        elif port is not None:
            self.ser = serial.Serial(port, 9600)
        else:
            raise Exception("Could not initialise without serial connection or port specification")

        # lock for writing to serial port
        self.lock = threading.Lock()

        # thread used for polling
        self._read_thread = self.Reader(kramer=self)
        self._read_thread.start()

        # state information
        # state[output] = input 
        self._video_state = {}
        for i in range(1, 9):
            self._video_state[i] = 0

    def __del___(self):
        # close serial port
        self.lock.aquire()
        self.ser.close()

    ########################
    # High Level Functions #
    ########################

    def switch_video_via_black(self, input_, output):
        self.switch_video(0, output)
        # may need to pause here
        time.sleep(0.1)
        self.switch_video(input_, output)

    def get_video_state(self):
        """ Returns a dictionary of (output => input) pairs """
        # TODO: should we return a copy?
        return self._video_state

    ################################
    # Communications Protocol 2000 #
    ################################

    # OPCODES
    OP_CODES = {'SWITCH_VIDEO': 1,
                'SWITCH_AUDIO': 2,
                'STORE_VIDEO_STATUS': 3,
                'RECALL_VIDEO_STATUS': 4}
    INSTR_SWITCH_VIDEO = 1
    INSTR_SWITCH_AUDIO = 2
    INSTR_STORE_VIDEO_STATUS = 3
    INSTR_RECALL_VIDEO_STATUS = 4

    def switch_video(self, input_, output):
        # construct string to send
        # byte1 = 0x01
        # byte2 = 0x80
        # byte3 = 0x80
        # byte4 = 0x81

        # byte3 += output
        # byte2 += input_
        # sendstring = array.array('B',  [byte1, byte2, byte3, byte4]).tostring()

        string = self._construct_message(1, input_, output)
        self._send_message(string)

        self._video_state[output] = input_

    def switch_audio(self, input, output):
        raise NotImplementedError()

    def store_video_status(self, setup_no, store_or_delete):
        assert store_or_delete in [0, 1]
        raise NotImplementedError()

    def recall_video_status(self, setup_no):
        raise NotImplementedError()

    def request_video_output_status(self, setup_no, output):
        raise NotImplementedError()

    def request_audio_output_status(self, setup_no, output):
        raise NotImplementedError()

    def vis_source(self, *args):
        raise NotImplementedError()

    def breakaway_setting(self, *args):
        raise NotImplementedError()

    def type_setting(self, video_or_audio, setting):
        raise NotImplementedError()

    def request_vis_setting(self, *args):
        raise NotImplementedError()

    def request_breakaway_setting(self, *args):
        raise NotImplementedError()

    def request_type_setting(self, *args):
        raise NotImplementedError()

    def set_highest_machine_address(self, video_or_audio, address):
        assert video_or_audio in [0, 1]
        raise NotImplementedError()

    def request_highest_machine_address(self, video_or_audio):
        assert video_or_audio in [0, 1]
        raise NotImplementedError()

    def request_whether_setup_is_defined_or_whether_valid_input_is_detected(self, setup_or_input_no, defined_or_valid):
        """ No seriously, this is an instruction """
        raise NotImplementedError()

    def reset_audio(self):
        raise NotImplementedError()

    def store_audio_status(self, setup_no, store_or_delete):
        assert store_or_delete in [0, 1]
        raise NotImplementedError()

    def recall_audio_status(self, setup_no):
        raise NotImplementedError()

    def set_video_parameter(self, port_number, parameter):
        raise NotImplementedError()

    def set_audio_parameter(self, port_number, parameter):
        raise NotImplementedError()

    def increase_or_decrease_video_parameter(self, port_number, parameter):
        raise NotImplementedError()

    def increase_or_decrease_audio_parameter(self, port_number, parameter):
        raise NotImplementedError()

    def request_audio_parameter(self, port_number):
        raise NotImplementedError()

    def request_video_parameter(self, port_number):
        raise NotImplementedError()

    def lock_front_panel(self, lock_or_unlock):
        assert lock_or_unlock in [0, 1]
        raise NotImplementedError()

    def request_front_panel_lock_status(self):
        raise NotImplementedError()

    def direct_memory_save(self, address, value):
        """ Editors note: How can this be a good idea? """
        raise NotImplementedError()

    def switch_control_data(self, input, output):
        raise NotImplementedError()

    def request_control_data_output_status(self, output):
        raise NotImplementedError()

    def change_to_ascii(self, protocol):
        assert protocol in [1, 2, 3]
        raise NotImplementedError()

    def set_autosave(self, enable_or_disable):
        assert enable_or_disable in [13, 14]
        raise NotImplementedError()

    def execute_loaded_data(self, setup_no, take_or_cancel):
        assert take_or_cancel in [1, 2]
        raise NotImplementedError()

    def load_video_data(self, input, output):
        raise NotImplementedError()

    def load_audio_data(self, intput, output):
        raise NotImplementedError()

    def identify_machine(self, request_item, request_part):
        assert request_item in range(1, 12)
        assert request_part in [0, 1, 2, 3, 10, 11, 12]
        raise NotImplementedError()

    def define_machine(self, *args):
        raise NotImplementedError()

    #######################
    # Low Level Functions #
    #######################

    def _construct_message(self, instruction, input_, output, machine_number=0x01, override=False):
        """ Returns a string suitable for sending to the machine """
        bytes = [0x00 + (instruction & 0x3F),
                 0x80 + (input_ & 0x7F),
                 0x80 + (output & 0x7F),
                 0x80 + (machine_number & 0x1F),
                 ]

        if override:
            bytes[3] += 0x40

        string = ''.join(map(chr, bytes))
        print ''.join(map(lambda x: x.encode('hex').upper(), string))
        return string

    def _send_message(self, message_string):
        # send message after acquiring lock
        with self.lock:
            x = self.ser.write(message_string)


class TestSerial(object):
    def __init__(self):
        self.closed = False

    def close(self):
        self.closed = True

    def read(self, bytes=1):
        if self.closed:
            raise Exception("Port is closed!")

        time.sleep(2)
        return [chr(0xAB)] * bytes

    def write(self, string):
        if self.closed:
            raise Exception("Port is closed!")

        print "Sending Message '%s' (0x%s)" % (string, ''.join(map(lambda x: x.encode('hex').upper(), string)))
