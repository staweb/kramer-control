# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoutingPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('slug', models.CharField(max_length=100)),
                ('allowed_inputs', models.ManyToManyField(to='api.InputChannel')),
                ('allowed_outpus', models.ManyToManyField(to='api.OutputChannel')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
