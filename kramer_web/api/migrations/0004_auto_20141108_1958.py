# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20141108_1946'),
    ]

    operations = [
        migrations.AddField(
            model_name='inputchannel',
            name='is_blank',
            field=models.BooleanField(default=True, help_text=b'True if nothing is plugged in'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='outputchannel',
            name='is_blank',
            field=models.BooleanField(default=True, help_text=b'True if nothing is plugged in'),
            preserve_default=True,
        ),
    ]
