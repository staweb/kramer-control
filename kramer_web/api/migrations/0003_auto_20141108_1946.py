# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_routingpage'),
    ]

    operations = [
        migrations.RenameField(
            model_name='routingpage',
            old_name='allowed_outpus',
            new_name='allowed_outputs',
        ),
    ]
