from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('',
    url(r"^api/switch_video/(?P<output_number>[0-9])/(?P<input_number>[0-9])/", views.switch_video),
    url(r"^api/get_video_state/$", views.get_state),
    url(r"^page/(?P<slug>\w+)/", views.routing_page),
)
