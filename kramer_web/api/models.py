from django.db import models


class MatrixChannel(models.Model):
    number = models.PositiveIntegerField(unique=True,
                                         help_text="The number of the port the source/output is"
                                         )
    label = models.CharField(max_length=100,
                             help_text="The title of the channel")
    is_blank = models.BooleanField(default=True,
                                   help_text="True if nothing is plugged in")

    class Meta:
        abstract = True
        ordering = ["number"]

    def __unicode__(self):
        return u"[" + unicode(self.number) + u"] " + self.label


class InputChannel(MatrixChannel):
    pass


class OutputChannel(MatrixChannel):
    pass


class RoutingPage(models.Model):
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100)
    allowed_inputs = models.ManyToManyField(InputChannel)
    allowed_outputs = models.ManyToManyField(OutputChannel)

    def __unicode__(self):
        return u"Page for '%s' at '%s'" % (self.name, self.slug)
