import json

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse
from django.template.defaulttags import register
import settings

import models
from kramer import Kramer, TestSerial


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


switcher = Kramer(port=settings.SERIAL_PORT)


def routing_page(request, slug):
    page = get_object_or_404(models.RoutingPage, slug=slug)

    num_inputs = len(page.allowed_inputs.all())
    num_outputs = len(page.allowed_outputs.all())

    col_width = 80 / num_inputs

    state = switcher.get_video_state()

    return render_to_response("routingpage.html", locals())


def switch_video(request, output_number, input_number):
    switcher.switch_video(int(input_number), int(output_number))
    return HttpResponse(json.dumps({"result": "success"}),
                        content_type="application/json")


def get_state(request):
    return HttpResponse(json.dumps(switcher.get_video_state()), content_type="application/json")
