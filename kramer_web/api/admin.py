from django.contrib import admin
from api import models


class InputChannelAdmin(admin.ModelAdmin):
    pass


class OutputChannelAdmin(admin.ModelAdmin):
    pass


class RoutingPageAdmin(admin.ModelAdmin):
    pass

admin.site.register(models.InputChannel, InputChannelAdmin)
admin.site.register(models.OutputChannel, OutputChannelAdmin)
admin.site.register(models.RoutingPage, RoutingPageAdmin)
